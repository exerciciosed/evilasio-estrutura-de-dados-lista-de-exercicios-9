package Lista9.Questao13
import Lista9.NodeBin
import Lista9.BinarySearchTree

class BinaryTreeQuestao13 extends BinarySearchTree{
  
  //metodo da questao 13
  def antecessor(n : NodeBin): NodeBin = {
    var naux : NodeBin = n
    if(naux.left.!=(null)) {
      naux = naux.left
      while(naux.right.!=(null))
        naux = n.right
    }    
    else{
      naux = naux.dad
      while(naux.dad.left.!=(naux) || naux.dad.!=(null)) {
         naux = naux.dad 
      }
    }  
    naux
  }
}