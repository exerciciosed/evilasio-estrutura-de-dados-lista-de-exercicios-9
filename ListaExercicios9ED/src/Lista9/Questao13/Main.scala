package Lista9.Questao13
import Lista9.NodeBin
import Lista9.BinaryTree

object Main {
  def main(args: Array[String]){
    var arv = new BinaryTreeQuestao13
    arv.insertBST(1, "4")
    arv.insertBST(2, "2")
    arv.insertBST(5, "5")
    arv.insertBST(6, "6")
    arv.insertBST(4, "1")
    arv.insertBST(3, "3")
    arv.insertBST(7, "7")
    println(arv.antecessor(arv.search(arv.root, 4)).info)    
  }
}