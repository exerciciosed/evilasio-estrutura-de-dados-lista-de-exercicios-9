package Lista9

class BinarySearchTree extends BinaryTree{
  
  def insertBST(key: Int, info: Any, n: NodeBin = root) {
    n.==(null) match {
      case true => {var nodeaux = new NodeBin(key, info); root = nodeaux}
      case false => n.key.==(key) match {
                        case true => println("Chave ja existe")
                        case false => n.key.>(key) match{
                          case true => n.left.==(null) match{
                            case true => {var nodeaux = new NodeBin(key, info); n.left = nodeaux; nodeaux.dad = n}
                            case false => insertBST(key, info, n.left)
                            }                    
                          case false => n.right.==(null) match{
                            case true => {var nodeaux = new NodeBin(key, info); n.right = nodeaux; nodeaux.dad = n}
                            case false => insertBST(key, info, n.right)
                            }            
                        }
                    }
    } 
  }
  
  //search
  def search(n : NodeBin = root, key:Int) : NodeBin ={
    n.==(null) match {
      case true => return null
      case false => n.key.==(key) match {
                        case true => return n
                        case false => n.key.>(key) match{
                          case true => search(n.left, key)
                          case false => search(n.right, key)
                        }
                    }
    } 
  }
}