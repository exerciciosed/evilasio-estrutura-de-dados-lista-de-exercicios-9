package Lista9.Questao9
import Lista9.NodeBin
import Lista9.BinaryTree

object Main {
  def main(args: Array[String]){
    var arv = new BinaryTreeQuestao9
    arv.insert(1, "1")
    arv.insert(2, "2")
    arv.insert(3, "3")
    arv.insert(4, "4")
    arv.insert(5, "5")
    arv.insert(6, "6")
    arv.insert(7, "7")
    arv.printLevelLeafsToRoot(arv.root)    
  }
}