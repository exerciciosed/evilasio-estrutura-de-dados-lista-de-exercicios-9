package Lista9.Questao9
import Lista9.NodeBin
import Lista9.BinaryTree

class BinaryTreeQuestao9 extends BinaryTree {
 
   //Metodo Questao 9
  def printLevelLeafsToRoot(n : NodeBin){
    for(i<- height(n) until 0 by -1){
      println("Nivel: "+i)
      printLevel(n, i)
    }
      
  }
 
  def printLevel(n : NodeBin, level : Int){
    if(n == null) return
    if(level == 1) println(n.info)
    if(level.>(1))
      printLevel(n.left, level-1); printLevel(n.right, level-1)   
  } 
} 

