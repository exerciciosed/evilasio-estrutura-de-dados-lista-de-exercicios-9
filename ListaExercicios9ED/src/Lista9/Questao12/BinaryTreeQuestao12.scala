package Lista9.Questao12

import Lista9.NodeBin
import Lista9.BinaryTree

class BinaryTreeQuestao12 extends BinaryTree{
  //metodo da questao 12
  def exchange(n : NodeBin){
    if(n!=null){
      exchange(n.left)
      exchange(n.right)
      var naux = n.left
      n.left = n.right
      n.right = naux
    }
  }
}