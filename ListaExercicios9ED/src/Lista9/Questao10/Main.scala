package Lista9.Questao10
import Lista9.NodeBin
import Lista9.BinaryTree

object Main {
  def main(args: Array[String]){
    var arv = new BinaryTreeQuestao10
    arv.insertBST(1, "1")
    arv.insertBST(2, "2")
    arv.insertBST(3, "3")
    arv.insertBST(4, "4")
    arv.insertBST(5, "5")
    arv.insertBST(6, "6")
    arv.insertBST(7, "7")
    println(arv.searchInfo(arv.root, 2))    
  }
}