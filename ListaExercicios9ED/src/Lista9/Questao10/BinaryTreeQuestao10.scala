package Lista9.Questao10
import Lista9.NodeBin
import Lista9.BinarySearchTree

class BinaryTreeQuestao10 extends BinarySearchTree{
   
  //Metodo da Questao 10
  def searchInfo(n : NodeBin, key:Int) : Any ={
    val naux = search(n, key) 
    if(naux.!=(null)) return naux.info
    else return null
  }
  
}
