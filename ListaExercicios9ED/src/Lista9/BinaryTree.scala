package Lista9

class BinaryTree() {
  
  var root:NodeBin = null 
  
  def insert(key: Int, info: Any, n: NodeBin = root) {
    n.==(null) match {
      case true => {var nodeaux = new NodeBin(key, info); root = nodeaux}
      case false => n.key.==(key) match {
                        case true => println("Chave ja existe")
                        case false => {
                                        if(n.left.==(null)) {
                                                    var nodeaux = new NodeBin(key, info) 
                                                    n.left = nodeaux 
                                                    nodeaux.dad = n
                                        }
                                        else if(n.right.==(null)) {
                                                    var nodeaux = new NodeBin(key, info); 
                                                    n.right = nodeaux
                                                    nodeaux.dad = n
                                        }
                                        else { 
                                            if(height(n.left).==(height(n.right)) && (numSon(n.left).<(2) || numSon(n.right).==(2))) 
                                                insert(key, info, n.left)
                                            else insert(key, info, n.right)   
                                        }
                                      }  
                        }
                    }
    } 
  
  
  //heigh  
  def height(n: NodeBin = root) : Int = {
    if(n.==(null)) return 0
    else{
      var heightL = height(n.left)
      var heightR = height(n.right)
      
      heightL.>(heightR) match {
        case true => heightL+1
        case false => heightR+1
      }
    }   
  }
  
  // numero de filhos
  def numSon(n: NodeBin): Int ={
    var count:Int =0
    if(n.left.!=(null)) count+=1
    if(n.right.!=(null)) count+=1
    count
  }
}