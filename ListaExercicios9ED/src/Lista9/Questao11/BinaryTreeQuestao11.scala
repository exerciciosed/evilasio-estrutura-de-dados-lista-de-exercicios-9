package Lista9.Questao11
import Lista9.NodeBin
import Lista9.BinaryTree

class BinaryTreeQuestao11 extends BinaryTree{
  var nodeMax: NodeBin = null
  //Metodo Questao 11 
  def searchMax(n : NodeBin, nMax: NodeBin = null) : NodeBin ={
    nodeMax = nMax
    n.==(null) match {
      case true => nodeMax
      case false => {   
                        if(n.dad.==(null)) nodeMax = n
                        if(nodeMax.key.<(n.key)){nodeMax = n}
                        searchMax(n.left, nodeMax)
                        searchMax(n.right, nodeMax)
                    }                       
    } 
  }
  
  
}
