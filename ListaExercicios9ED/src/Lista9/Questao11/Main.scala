package Lista9.Questao11
import Lista9.NodeBin
import Lista9.BinaryTree

object Main {
  def main(args: Array[String]){
    var arv = new BinaryTreeQuestao11
    arv.insert(1, "1")
    arv.insert(2, "2")
    arv.insert(3, "3")
    arv.insert(4, "4")
    arv.insert(200, "200")
    arv.insert(6, "6")
    arv.insert(7, "7")
    println(arv.searchMax(arv.root).info) 
  }
}